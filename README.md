## Description
Simple web servers draw based on Andrew Mead's tutorials.

## Features: ##

    1. Serving up dynamic pages using express.js.
    2. Rendering content using handlebars templates.
    
    
## Author information ##

Ernest Kost

ernest.kost@gmail.com



## Installation and launching ##


### 1. Installing npm packages ###

  In order to launch the app, firstly you need to install the required npm packages.
  Change your directory to folder with cloned repo then use the command:
  
  `npm install`

  
### 2. Launching the app ###

  In order to see website on local machine go to main folder (where the package.json is) and use command:
  
  `npm run start`
  
  and open `http://localhost:3000/` in your browser
  
  
###  Used tools and frameworks ###

  
  - [Express](https://expressjs.com)
  - [Handlebars](https://handlebarsjs.com/)